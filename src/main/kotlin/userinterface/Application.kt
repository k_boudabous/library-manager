package userinterface

import entities.Library
import external.Export
import usecases.Management

fun main(args: Array<String>) {
    val export = Export()
    val management = Management(export)
    while (true) {
        println("choisissez l'action à effectuer : ajouter / lister / compter / supprimer / fichier")
        val action = readLine()
        if (action == "ajouter") {
            management.addBookToLibrary()
        }
        if (action == "lister") {
            management.listBooks()
        }
        if (action == "compter") {
            println(management.countBooks())
        }
        if (action == "supprimer") {
            println("quel est l'id du livre à supprimer")
            val livreId = readLine()
            livreId?.let { management.removeBooks(it) }
        }
        if (action == "fichier"){
            management.exportLibrary(management.library)
        }
    }
}