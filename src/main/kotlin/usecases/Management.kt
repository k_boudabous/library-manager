package usecases

import entities.Book
import entities.Library
import external.ExportInterface
import java.util.*


class Management(val exportInterface: ExportInterface) {

    val library: Library = Library()

    fun addBookToLibrary() {
        println("« Bienvenue dans votre bibliothèque. Pour ajouter un livre, appuyez sur une touche. »")
        println("« Saisissez les informations du livre à ajouter »")
        println("« Auteur : »")
        var author = readLine()
        println("« Titre : »")
        var title = readLine()
        println("« Genre : »")
        var genre = readLine()
        val book = Book(author = author, title = title, genre = genre)
        book?.let { library.books?.add(book) }
        println("livre ajouté !")
    }

    fun listBooks() {
        for (book in library.books!!) {
            println("livre : ")
            println(library.bookToString(book))
        }
    }

    fun removeBooks(idToRemove: String) {
        library.books?.removeAll { it.id!!.equals(UUID.fromString(idToRemove))}
    }

    fun countBooks(): Int? {
        return library.books?.size
    }

    fun exportLibrary(library: Library) {
        exportInterface.exportLibrary(library)
    }
}