package test.kotlin

import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition


@AnalyzeClasses(packagesOf = [EntitiesTest::class])
class EntitiesTest {

    @ArchTest
    val entitiesNotDependOnExternal = ArchRuleDefinition.noClasses().that().resideInAPackage("..entities..")
            .should().dependOnClassesThat().resideInAPackage("..external..")


    @ArchTest
    val entitiesNotDependOnusecases =   ArchRuleDefinition.noClasses().that().resideInAPackage("..entities..")
            .should().dependOnClassesThat().resideInAPackage("..usecases..")


    @ArchTest
    val entitiesNotDependOnuserinterface =  ArchRuleDefinition.noClasses().that().resideInAPackage("..entities..")
            .should().dependOnClassesThat().resideInAPackage("..userinterface..")


}
