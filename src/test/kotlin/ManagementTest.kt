package test.kotlin

import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition

@AnalyzeClasses(packagesOf = [ManagementTest::class])
class ManagementTest {

    @ArchTest
    val useCaseOnlyDependOnEntities =
        ArchRuleDefinition.classes().that().resideInAPackage("..usecases..")
            .should().onlyHaveDependentClassesThat().resideInAnyPackage("..entities..")

}