package entities

import java.util.UUID

class Book(val author: String?, val title: String?, val genre: String?) {

    var id: UUID? = null

    init {
        this.id = UUID.randomUUID()
    }
}