package external

import entities.Library
import java.io.PrintWriter

class Export : ExportInterface{


    override fun exportLibrary(library: Library) {
        val writer = PrintWriter("file.txt")

        for (book in library.books!!) {
            writer.append(library.bookToString(book))
        }

        writer.close()
    }
}